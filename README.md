# MUD

## INSTALLATION

  create a virtualenv:

```bash
$ virtualenv -p python3 venv
$ source venv/bin/activate
$ pip3 install tornado==4.5.3
$ pip3 install pyyaml
```

## RUNNING

```bash
$ ./mud.py
```

## USING

  direct your browser to http://localhost:9999/
  The server actually listens on all interfaces.
